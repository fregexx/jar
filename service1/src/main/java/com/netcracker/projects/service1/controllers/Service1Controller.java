package com.netcracker.projects.service1.controllers;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import com.netcracker.projects.service1.config.BasicConfiguration;
import com.netcracker.projects.service1.models.Data;
import com.netcracker.projects.service1.services.SomeService;

import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.LogManager;

@RefreshScope
@RestController
//@Configuration
//@PropertySource("file://${CONFIG_LOCATION}/application.properties")
//@EnableAutoConfiguration
//@PropertySource("file:///C:/Users/alsm0918/Desktop/application.properties")
public class Service1Controller {

    private static Logger logger = (Logger)LoggerFactory.getLogger(Service1Controller.class);

    @Autowired
    private BasicConfiguration basicConfiguration;

    @Autowired
    private SomeService service;

    @Value("${spring.application.name}")
    String serviceName;

    @RequestMapping("/hi")
    public String hi(ModelMap model) throws InterruptedException {
        System.out.println("hi method");
        return "Hello. Service name: "+ serviceName + basicConfiguration;
    }

    @RequestMapping("")
    public String hello(ModelMap model) throws InterruptedException {
        System.out.println("hello method");
        logger.info("hello method in SomeController");   // [application name, traceId, spanId, export]
        String response = "hello";
        response = service.sendGet();

        LoggerContext loggerContext = logger.getLoggerContext();
        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(loggerContext);
        encoder.setPattern("%-5level [%thread]: %message%n");
        encoder.start();

        ConsoleAppender<ILoggingEvent> appender = new ConsoleAppender<ILoggingEvent>();
        appender.setContext(loggerContext);
        appender.setEncoder(encoder);
        appender.start();

        //logger.addAppender(appender);

        //logger.info("error!!!");

        return response;
    }

    @RequestMapping(value = "/post", method = RequestMethod.GET)
    public Data post() throws InterruptedException {
        Data response = service.sendPost();
        return response;
    }
}
