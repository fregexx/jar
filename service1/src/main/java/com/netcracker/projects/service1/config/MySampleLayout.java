package com.netcracker.projects.service1.config;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.LayoutBase;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class MySampleLayout extends LayoutBase<ILoggingEvent> {

    String prefix = null;
    boolean printThreadName = true;

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setPrintThreadName(boolean printThreadName) {
        this.printThreadName = printThreadName;
    }

    @Value("${spring.application.name:unknown-spring-boot}")
    private String serviceName = "service1";

    private Map<String, Boolean> config = new LinkedHashMap<String, Boolean>();
    Level level = Level.INFO;

    public void initConfig() {
        config.put("level", true);
        config.put("message", true);
        config.put("source", true);
        //config.put("exception", false);
        //config.put("trace", true);
        //config.put("Environment Name", true);
        config.put("Application Name", true);
        config.put("X-B3-TraceId", true);
        config.put("X-B3-SpanId", true);
        config.put("X-Span-Export", true);
        config.put("PID", true);
        //config.put("thread", true);
        //config.put("category", true);
        //config.put("code", true);

    }
    @Override
    public String doLayout(ILoggingEvent event) {
        initConfig();
        Map<String, String> patterns = new HashMap<String, String>();
        patterns.put("message", "%message");
        patterns.put("level", "%level");
        patterns.put("source", "%logger{40}");
        patterns.put("exception", "%ex");
        patterns.put("trace", "%ex{short}");
        patterns.put("Environment Name", "env");
        patterns.put("Application Name", serviceName);
        patterns.put("X-B3-TraceId", "%X{X-B3-TraceId:-}");
        patterns.put("X-B3-SpanId", "%X{X-B3-SpanId:-}");
        patterns.put("X-Span-Export", "%X{X-Span-Export:-}");
        patterns.put("PID", "${PID:-}");
        patterns.put("thread", "%thread");
        patterns.put("category", "category");
        patterns.put("code", "code");
        //Logger rootLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        //LoggerContext loggerContext = rootLogger.getLoggerContext();
        // we are not interested in auto-configuration
        //loggerContext.reset();

        Map<String, String> outputLog = new HashMap<String, String>();

        config.forEach((k, v) -> {
            if (v) outputLog.put(k, patterns.get(k));
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonLog = "";
        try {
            jsonLog = mapper.writeValueAsString(outputLog);
            //System.out.println(jsonLog);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        String pattern;
        //Map<String, String> log = new HashMap<String, String>();
        StringBuffer sbuf = new StringBuffer(128);
/*
        sbuf.append(event.getTimeStamp() - event.getLoggerContextVO().getBirthTime());
        sbuf.append("%level");
        sbuf.append(event.getLevel());
        sbuf.append(" [");
        sbuf.append(event.getThreadName());
        sbuf.append("] ");
        sbuf.append(event.getLoggerName());
        sbuf.append(" - ");
        sbuf.append(event.getFormattedMessage());
        sbuf.append(CoreConstants.LINE_SEPARATOR);
        return sbuf.toString();*/
        jsonLog += "\n";
        return jsonLog;
    }
}
