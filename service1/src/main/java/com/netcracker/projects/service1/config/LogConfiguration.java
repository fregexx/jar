package com.netcracker.projects.service1.config;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.CoreConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.logstash.logback.composite.JsonProviders;
import net.logstash.logback.composite.loggingevent.LoggingEventJsonProviders;
import net.logstash.logback.composite.loggingevent.LoggingEventPatternJsonProvider;
import net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class LogConfiguration {

    private static Logger logger = (Logger) LoggerFactory.getLogger(LogConfiguration.class);

    private Map<String, Boolean> config = new LinkedHashMap<String, Boolean>();
    Level level = Level.INFO;

    //@EventListener(ApplicationReadyEvent.class)
    //@EventListener(ContextRefreshedEvent.class)
    public void configure(){
        initConfig();
        setPattern();
    }
    public void initConfig() {
        config.put("message", true);
        config.put("source", true);
        config.put("exception", true);
        config.put("trace", true);
        config.put("Environment Name", true);
        config.put("Application Name", true);
        config.put("X-B3-TraceId", true);
        config.put("X-B3-SpanId", true);
        config.put("X-Span-Export", true);
        config.put("PID", true);
        config.put("thread", true);
        config.put("category", true);
        config.put("code", true);
    }

    @Value("${spring.application.name:unknown-spring-boot}")
    private String serviceName;

    public void setPattern() {

        Map<String, String> patterns = new LinkedHashMap<String, String>();

        patterns.put("timestamp", "%date{dd-mm-yyyy HH:mm:ss.SSS}");
        patterns.put("level", "%level");
        patterns.put("Application Name", serviceName);
        patterns.put("source", "%logger{40}");
        patterns.put("PID", "${PID:-}");
        patterns.put("pid", "$PID");
        patterns.put("exception", "ex");
        patterns.put("X-B3-TraceId", "%X{X-B3-TraceId:-}");
        patterns.put("X-B3-SpanId", "%X{X-B3-SpanId:-}");
        patterns.put("X-Span-Export", "%X{X-Span-Export:-}");
        patterns.put("message", "%message");
        patterns.put("trace", "%ex{short}");
        patterns.put("Environment Name", "env");
        patterns.put("thread", "%thread");
        patterns.put("category", "category");
        patterns.put("code", "code");



        ObjectMapper mapper = new ObjectMapper();
        String jsonInString="";
        try {
            jsonInString = mapper.writeValueAsString(patterns);
            System.out.println(jsonInString);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        String pattern;

        //config.forEach();

        Logger rootLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        LoggerContext loggerContext = rootLogger.getLoggerContext();
        // we are not interested in auto-configuration
        //loggerContext.reset();
        logger.setLevel(level);

        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(loggerContext);
        //encoder.setPattern("%-5level + 777 %n");
        jsonInString.concat(CoreConstants.LINE_SEPARATOR);
        encoder.setPattern(jsonInString);
        encoder.start();

        ConsoleAppender<ILoggingEvent> appender = new ConsoleAppender<ILoggingEvent>();
        appender.setContext(loggerContext);
        appender.setEncoder(encoder);
        appender.start();

        rootLogger.addAppender(appender);
        System.out.println();

    }
}
