package com.netcracker.projects.service1.config;

//import brave.Tracing;
//import brave.opentracing.BraveTracer;
//import brave.opentracing.BraveTracer;
//import brave.sampler.Sampler;
//import io.jaegertracing.internal.JaegerTracer;
//import io.jaegertracing.internal.samplers.ProbabilisticSampler;
//import io.opentracing.Tracer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@Configuration
public class SpringConfig {

    @Bean
    RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    String getService2URL() {
        return "http://localhost:8092";
    }

    //@Value("${jaeger.service.name}")
    //String jaegerServiceName;

    //@Bean
    /*public io.opentracing.Tracer jaegerTracer() {
        io.jaegertracing.Configuration configuration = io.jaegertracing.Configuration.fromEnv(jaegerServiceName);
        return configuration.getTracer();
    }*/
    /*
    //@Bean
    Tracer braveTracer(Tracing braveTracing) {
        return BraveTracer.create(braveTracing);
        6831/api/traces
        6831
        14268/api/traces
        14268/api/traces/

    }*/
    /*
    @Bean
    public io.opentracing.Tracer zipkinTracer() {
//        OkHttpSender okHttpSender = OkHttpSender.create("https://jaeger-collector:6831/api/traces");
        OkHttpSender okHttpSender = OkHttpSender.create("http://localhost:6831");
        AsyncReporter<Span> reporter = AsyncReporter.builder(okHttpSender).build();
        Tracing braveTracing = Tracing.newBuilder().localServiceName(serviceName).spanReporter(reporter).build();
        return BraveTracer.create(braveTracing);
    }
*/
    @Value("${spring.application.name:unknown-spring-boot}")
    private String serviceName;
/*
    //@Bean
    public io.opentracing.Tracer jaegerTracer() {
        io.jaegertracing.Configuration.SamplerConfiguration samplerConfiguration = new io.jaegertracing.Configuration.SamplerConfiguration();
        samplerConfiguration.withType(ProbabilisticSampler.TYPE);
        samplerConfiguration.withParam(1);
        io.jaegertracing.Configuration.ReporterConfiguration reporterConfiguration = new io.jaegertracing.Configuration.ReporterConfiguration();
        io.jaegertracing.Configuration configuration = new io.jaegertracing.Configuration(this.serviceName);
        configuration.withSampler(samplerConfiguration);
        configuration.withReporter(reporterConfiguration);
        return configuration.getTracer();
    }*/
}
