package com.netcracker.projects.service1.services;

import com.netcracker.projects.service1.models.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SomeService {

    private static Logger logger = LoggerFactory.getLogger(SomeService.class);

    @Autowired
    private String service2URL;

    @Autowired
    private RestTemplate restTemplate;

    public String sendGet() throws InterruptedException {
        logger.info("sendGet method");

//        Span span = tracer.nextSpan().annotate("annotation").name("name").tag("tag", "tag-value").start();
//        try (Tracer.SpanInScope ws = tracer.withSpanInScope(span.start())) {
//            Thread.sleep(1000L);
//            logger.info("I'm in the new span doing some cool work that needs its own span");
//        } finally {
//            span.finish();
//        }
//        TraceContext context = span.context();


        String response = restTemplate.getForObject(service2URL + "/service2-get", String.class);
        //TraceContext context = tracer.currentSpan().context();
//        tracer.currentSpan().name("123").
        return response;
    }

    public Data sendPost() throws InterruptedException {
        logger.info("sendPost method");

        Data data = new Data(2, "data2");
        Data response = restTemplate.postForObject(service2URL + "/post", data, Data.class);
        String s = sendGet();

        return response;
    }
}
