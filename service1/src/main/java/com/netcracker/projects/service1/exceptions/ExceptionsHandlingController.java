package com.netcracker.projects.service1.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class ExceptionsHandlingController {

    // Convert a predefined exception to an HTTP Status code
    @ExceptionHandler(InterruptedException.class)
    public void conflict() {
        // Nothing to do
        System.out.println("error handler");
    }
}


