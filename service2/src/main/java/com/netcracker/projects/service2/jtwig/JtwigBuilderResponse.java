package com.netcracker.projects.service2.jtwig;

public class JtwigBuilderResponse {
    private String html;

    public JtwigBuilderResponse(String html) {
        this.html = html;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    @Override
    public String toString() {
        return "JtwigBuilderResponse{" +
                "html='" + html + '\'' +
                '}';
    }
}
