package com.netcracker.projects.service2.jtwig;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JtwigBuilderController {

    @RequestMapping(method = RequestMethod.POST, path = "/twig")
    public JtwigBuilderResponse buildTemplate(@RequestBody JtwigBuilderRequest request) {
        String builtHtml = JtwigBuilderService.build(request);
        return new JtwigBuilderResponse(builtHtml);
    }
}
