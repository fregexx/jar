package com.netcracker.projects.service2.controllers;

import com.netcracker.projects.service2.models.Data;
import com.netcracker.projects.service2.services.MyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Service2Controller {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MyService service;

    @RequestMapping("/service2-get")
    public String hello(ModelMap model){
        logger.info("Hello. I am service #2");   //[application name, traceId, spanId, export]
        String response = "Hello. I am service #2";
        return response;
    }

    @RequestMapping("/service2-post")
    public Data post(@RequestBody Data data){
        if(data != null)
            return data;
        return new Data(0,"Data = null");
    }
}
