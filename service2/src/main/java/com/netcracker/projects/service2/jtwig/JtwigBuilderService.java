package com.netcracker.projects.service2.jtwig;

import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

public class JtwigBuilderService {

    public static String build(JtwigBuilderRequest request) {
        JtwigTemplate template = JtwigTemplate.inlineTemplate(request.getTemplate());
        JtwigModel model = JtwigModel.newModel(request.getContext());
        return template.render(model);
    }
}
