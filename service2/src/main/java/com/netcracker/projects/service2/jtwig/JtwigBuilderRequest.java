package com.netcracker.projects.service2.jtwig;

import java.util.Map;

public class JtwigBuilderRequest {
    private String template;

    private Map<String, Object> context;

    protected JtwigBuilderRequest() {

    }

    public JtwigBuilderRequest(String template, Map<String, Object> context) {
        this.template = template;
        this.context = context;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Map<String, Object>  getContext() {
        return context;
    }

    public void setContext(Map<String, Object>  context) {
        this.context = context;
    }
}
