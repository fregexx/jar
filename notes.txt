Jaeger
Collector service exposes Zipkin compatible REST API /api/v1/spans and /api/v2/spans for both JSON and thrift encoding.
By default it’s disabled. It can be enabled with --collector.zipkin.http-port=9411.

C:\Users\Smirn\.m2\repository\org\springframework\cloud\spring-cloud-sleuth-core\2.0.1.RELEASE\spring-cloud-sleuth-core-2.0.1.RELEASE.jar
spring-cloud-sleuth-core
org.springframework.cloud.sleuth.autoconfig
TraceAutoConfiguration returns brave.tracer

io.opentracing.contrib.java.spring.jaeger.starter;
JaegerAutoConfiguration